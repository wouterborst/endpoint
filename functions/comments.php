<?php
/**
 * Return Comments data
 *
 * @param none
 * @return object|null Wordpress Comments object,  * or null if none.
 * @since 1.3.0
 */

function show_comments( $post_ID = false ) {

    // check if has post id
    if ( $post_ID ) {
        
        // Set variables
        $data = [];
        $dataList = [];

        $comments = get_comments($post_ID);

        if ($comments) {
            
            foreach($comments as $item) {

                if ($item->comment_post_ID == $post_ID) {
                    
                    $dataList['id'] = $item->comment_ID;
                    $dataList['post_id'] = $item->comment_post_ID;
                    $dataList['date'] = $item->comment_date;
                    $dataList['content'] = $item->comment_content;
                    $dataList['author']['name'] = $item->comment_author;
                    $dataList['author']['email'] = $item->comment_author_email;
                    $dataList['author']['url'] = $item->comment_author_url;

                    array_push($data, $dataList);

                }
            }
        }
        
        return $data;

    }

}