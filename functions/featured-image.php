<?php
/**
 * Return Featured image url data
 *
 * @param none
 * @return object|null Wordpress Featured image object,  * or null if none.
 * @since 1.3.0
 */

function show_featured_image( $post_ID = false ) {

    // check if has post id
    if ( $post_ID ) {
        
        // Set variables
        $data = [];

        $data['thumbnail'] = get_the_post_thumbnail_url($post_ID, 'thumbnail');
	    $data['medium'] = get_the_post_thumbnail_url($post_ID, 'medium');
        $data['large'] = get_the_post_thumbnail_url($post_ID, 'large');
  
        return $data;

    }

}