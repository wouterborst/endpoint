<?php
/**
 * Return Tags data
 *
 * @param none
 * @return object|null Wordpress Tags object,  * or null if none.
 * @since 1.3.0
 */

function show_categories( $post_ID = false ) {

    // check if has post id
    if ( $post_ID ) {
        
        // Set variables
        $data = [];
        $posttags = get_the_tags($post_ID);


        $postcategory = get_the_category($post_ID);
        if ($postcategory) {
            
            $key = 0;
            foreach($postcategory as $key => $tag) {
                $data[$key]['id'] = $tag->term_id;
                $data[$key]['name'] = $tag->name;
                $data[$key]['slug'] = $tag->slug;  
            }
        }
            
        return $data;

    }

}