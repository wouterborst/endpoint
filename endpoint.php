<?php
/**
 * Plugin Name: Endpoint API
 * Plugin URI: http://wouterborst.com
 * Description: Wordpress Endpoint that simplify the data you retrieve from WordPress
 * Version: 1.5.1
 * Author: Wouter Borst
 * Author URI: http://wouterborst.com
 */

/* Functions */
require_once 'functions/featured-image.php';
require_once 'functions/yoast.php';
require_once 'functions/tags.php';
require_once 'functions/categories.php';
require_once 'functions/comments.php';

/* Endpoints */
require_once 'endpoints/pages.php';
require_once 'endpoints/posts.php';
require_once 'endpoints/categories.php';
require_once 'endpoints/tags.php';

add_action(
	'rest_api_init',
	function () {

		if ( ! function_exists( 'use_block_editor_for_post_type' ) ) {
			require ABSPATH . 'wp-admin/includes/post.php';
		}

		// Surface all Gutenberg blocks in the WordPress REST API
		$post_types = get_post_types_by_support( [ 'editor' ] );
		foreach ( $post_types as $post_type ) {
			if ( use_block_editor_for_post_type( $post_type ) ) {
				register_rest_field(
					$post_type,
					'blocks',
					[
						'get_callback' => function ( array $post ) {
							return parse_blocks( $post['content']['raw'] );
						},
					]
				);
			}
		}
	}
);

add_action('rest_api_init', function() {
    
    // Posts
    register_rest_route('endpoint/v1', 'posts', [
		'methods' => 'GET',
		'callback' => 'wl_posts',
		'args' => [
			'post_per_page',
			'paged'
		]
	]);
	register_rest_route( 'endpoint/v1', 'posts/(?P<slug>[a-zA-Z0-9-]+)', array(
		'methods' => 'GET',
		'callback' => 'wl_post',
		function () {

			if ( ! function_exists( 'use_block_editor_for_post_type' ) ) {
				require ABSPATH . 'wp-admin/includes/post.php';
			}
	
			// Surface all Gutenberg blocks in the WordPress REST API
			$post_types = get_post_types_by_support( [ 'editor' ] );
			foreach ( $post_types as $post_type ) {
				if ( use_block_editor_for_post_type( $post_type ) ) {
					register_rest_field(
						$post_type,
						'blocks',
						[
							'get_callback' => function ( array $post ) {
								return parse_blocks( $post['content']['raw'] );
							},
						]
					);
				}
			}
		}
    ) );

    // Pages
    register_rest_route('endpoint/v1', 'pages', [
		'methods' => 'GET',
		'callback' => 'wl_pages',
	]);
	register_rest_route( 'endpoint/v1', 'pages/(?P<slug>[a-zA-Z0-9-]+)', array(
		'methods' => 'GET',
        'callback' => 'wl_page'
    ) );

    // Categories
    register_rest_route('endpoint/v1', 'categories', [
		'methods' => 'GET',
		'callback' => 'wl_categories',
	]);
    register_rest_route('endpoint/v1', 'categories/(?P<slug>[-\w]+)', array(
		'methods' => 'GET',
		'callback' => 'wl_category',
		'args' => [
			'post_per_page',
			'paged'
		]
    ) );
    
    // Tags
    register_rest_route('endpoint/v1', 'tags', [
		'methods' => 'GET',
		'callback' => 'wl_tags',
    ]);
    register_rest_route('endpoint/v1', 'tags/(?P<slug>[a-zA-Z0-9-]+)', array(
		'methods' => 'GET',
		'callback' => 'wl_tag',
		'args' => [
			'post_per_page',
			'paged'
		]
    ) );
    
});