<?php 

function wl_categories() {
	$args = [
		'numberposts' => 20,
        'orderby' => 'name',
    ];
    
	$categories = get_categories($args);
	$data = [];
    
    $i = 0;
	foreach($categories as $category) {
		$data[$i]['id'] = $category->term_id;
        $data[$i]['title'] = $category->name;
        $data[$i]['parent_id'] = $category->category_parent;
        $data[$i]['slug'] = $category->slug;
        $data[$i]['description'] = $category->description;        
		$i++;
	}
	return $data;
}

function wl_category( $slug ) {
    global $post;
    if ($slug['slug'] !== 'sri-lanka' ) {
        $args = [
            'name' => $slug['slug'],
            'post_type' => 'post',
        ];
    } else {
        $args = [
            'post_type' => 'post',
            'include' => 2157
        ];
    }
    
    $category = get_categories($args);
    
    $data['id'] = $category[0]->term_id;
    $data['title'] = $category[0]->name;
    $data['parent_id'] = $category[0]->category_parent;
    $data['slug'] = $category[0]->slug;
    $data['description'] = $category[0]->description;
    $data['post_count'] = $category[0]->count;
    
    $term = get_queried_object();
    $data['information'] = get_field( 'category.information', $category[0]->taxonomy . '_' . $category[0]->term_id );
    
    $linkTickets = get_field( 'category.tickets.link', $category[0]->taxonomy . '_' . $category[0]->term_id );
    $data['links'][0]['id'] = $linkTickets->ID;
    $data['links'][0]['name'] = 'category-slug';
    $data['links'][0]['title'] = 'Vliegtickets';
    $data['links'][0]['category'] = $category[0]->slug;
    $data['links'][0]['slug'] = get_permalink($linkTickets->ID);
    $data['links'][0]['icon'] = 'tickets';

    $linkPackinglist = get_field( 'category.packing.list.link', $category[0]->taxonomy . '_' . $category[0]->term_id );
    $data['links'][1]['id'] = $linkPackinglist->ID;
    $data['links'][1]['name'] = 'category-slug';
    $data['links'][1]['title'] = 'Paklijst';
    $data['links'][1]['category'] = $category[0]->slug;
    $data['links'][1]['slug'] = get_permalink($linkPackinglist->ID);
    $data['links'][1]['icon'] = 'packinglist';

    $linkRoute = get_field( 'category.travel.route.link', $category[0]->taxonomy . '_' . $category[0]->term_id );
    $data['links'][2]['id'] = $linkRoute->ID;
    $data['links'][2]['name'] = 'category-slug';
    $data['links'][2]['title'] = 'Reisroute';
    $data['links'][2]['category'] = $category[0]->slug;
    $data['links'][2]['slug'] = get_permalink($linkRoute->ID);
    $data['links'][2]['icon'] = 'route';

    $linkVisum = get_field( 'category.visum.link', $category[0]->taxonomy . '_' . $category[0]->term_id );
    $data['links'][3]['id'] = $linkVisum->ID;
    $data['links'][3]['name'] = 'category-slug';
    $data['links'][3]['title'] = 'Visum';
    $data['links'][3]['category'] = $category[0]->slug;
    $data['links'][3]['slug'] = get_permalink($linkVisum->ID);
    $data['links'][3]['icon'] = 'visum';

    $argsPosts = [
        'post_type' => 'post',
        'cat' => $category[0]->term_id,
        'numberposts' => $slug['post_per_page'],
        'paged' => $slug['paged']
    ];

    $posts = get_posts($argsPosts);

    $i = 0;
	foreach($posts as $post) {
		$data['posts'][$i]['id'] = $post->ID;
        $data['posts'][$i]['title'] = $post->post_title;
        $data['posts'][$i]['title_short'] = get_field( 'post.titleShort', $post->ID );
		$data['posts'][$i]['slug'] = $post->post_name;
		$data['posts'][$i]['featured_image']['thumbnail'] = get_the_post_thumbnail_url($post->ID, 'thumbnail');
		$data['posts'][$i]['featured_image']['medium'] = get_the_post_thumbnail_url($post->ID, 'medium');
        $data['posts'][$i]['featured_image']['large'] = get_the_post_thumbnail_url($post->ID, 'large');
        
        $posttags = get_the_tags($post->ID);
        if ($posttags) {
            $key = 0;
            foreach($posttags as $key => $tag) {
                $data['posts'][$i]['tags'][$key]['id'] = $tag->term_id;
                $data['posts'][$i]['tags'][$key]['name'] = $tag->name;
                $data['posts'][$i]['tags'][$key]['slug'] = $tag->slug;  
            }
        }

        $postcategory = get_the_category($post->ID);
        if ($postcategory) {
            $key = 0;
            foreach($postcategory as $key => $tag) {
                $data['posts'][$i]['categories'][$key]['id'] = $tag->term_id;
                $data['posts'][$i]['categories'][$key]['name'] = $tag->name;
                $data['posts'][$i]['categories'][$key]['slug'] = $tag->slug;  
            }
        }

        // comments
        $data['posts'][$i]['comments']['count'] = get_comments_number($post->ID);

        $i++;
    }
    
	return $data;
}