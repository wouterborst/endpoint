<?php

function wl_posts( $slug ) {
	$args = [
		'numberposts' => 12,
        'post_type' => 'post',
        'numberposts' => $slug['post_per_page'],
        'pages' => $slug['paged']
    ];
    
	$posts = get_posts($args);
	$data = [];
    
    $i = 0;
	foreach($posts as $i => $post) {
        // Define the page ID
        
        $id = $post->ID;
        $data[$i]['id'] = $post->ID;
        $data[$i]['title'] = $post->post_title;
        $data[$i]['title_short'] = get_field( 'post.titleShort', $post->ID );
		$data[$i]['slug'] = $post->post_name;
		$data[$i]['featured_image']['thumbnail'] = get_the_post_thumbnail_url($post->ID, 'thumbnail');
		$data[$i]['featured_image']['medium'] = get_the_post_thumbnail_url($post->ID, 'medium');
        $data[$i]['featured_image']['large'] = get_the_post_thumbnail_url($post->ID, 'large');    

        $posttags = get_the_tags($post->ID);
        if ($posttags) {
            $key = 0;
            foreach($posttags as $key => $tag) {
                $data[$i]['tags'][$key]['id'] = $tag->term_id;
                $data[$i]['tags'][$key]['name'] = $tag->name;
                $data[$i]['tags'][$key]['slug'] = $tag->slug;  
            }
        }
    
        $postcategory = get_the_category($post->ID);
        if ($postcategory) {
            $key = 0;
            foreach($postcategory as $key => $tag) {
                $data[$i]['categories'][$key]['id'] = $tag->term_id;
                $data[$i]['categories'][$key]['name'] = $tag->name;
                $data[$i]['categories'][$key]['slug'] = $tag->slug;  
            }
        }

        // Comments
        $data[$i]['comments']['count'] = get_comments_number($post->ID);

		$i++;
	}
	return $data;
}

function wl_post( $slug ) {
	$args = [
		'name' => $slug['slug'],
		'post_type' => 'post'
	];
    
    $post = get_posts($args);
    $id = $post[0]->ID;

	$data['id'] = $id;
    $data['title'] = $post[0]->post_title;
    $data['title_short'] = get_field( 'post.titleShort', $id );
    $data['slug'] = $post[0]->post_name;
    $data['date_modified'] = get_the_modified_date( 'd-m-Y', $id );
    $data['featured_image'] = show_featured_image($id);
    $data['tags'] = show_tags($id);
    $data['categories'] = show_categories($id);
    
    $dataBlocks = parse_blocks($post[0]->post_content);
    $newBlocks = [];

    if ($dataBlocks) {
            
        $key = 0;
        foreach($dataBlocks as $item) {
            if ($item['blockName'] !== null) {
                array_push($newBlocks, $item);
            }
        }
    }
    $data['blocks'] = $newBlocks;
    
    // Comments
    $data['comments']['list'] = show_comments($id);
    $data['comments']['count'] = get_comments_number($id);

    // Yoast data
    $data['seo'] = get_yoast($id);

	return $data;
}