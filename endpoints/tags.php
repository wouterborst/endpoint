<?php

function wl_tags() {
	$args = [
		'numberposts' => 10,
        'orderby' => 'name',
    ];
    
	$tags = get_tags($args);
	$data = [];
    
    $i = 0;

	foreach($tags as $tag) {
		$data[$i]['id'] = $tag->term_id;
        $data[$i]['title'] = $tag->name;
        $data[$i]['slug'] = $tag->slug;        
        $i++;
	}
	return $data;
}

function wl_tag( $slug ) {
	$args = [
        'name' => $slug['slug'],
        'post_type' => 'post',
        'numberposts' => 100 
    ];
        
    $tag = get_tags($args);

    $data['id'] = $tag[0]->term_id;
    $data['title'] = $tag[0]->name;
    $data['slug'] = $tag[0]->slug;
    $data['description'] = $tag[0]->description;
    
    $argsPosts = [
        'post_type' => 'post',
        'tag' => $tag[0]->slug,
        'numberposts' => $slug['post_per_page'],
        'paged' => $slug['paged']
    ];

    $posts = get_posts($argsPosts);

    $i = 0;
	foreach($posts as $post) {
		$data['posts'][$i]['id'] = $post->ID;
        $data['posts'][$i]['title'] = $post->post_title;
        $data['posts'][$i]['title_short'] = get_field( 'post.titleShort', $post->ID );
		$data['posts'][$i]['slug'] = $post->post_name;
		$data['posts'][$i]['featured_image']['thumbnail'] = get_the_post_thumbnail_url($post->ID, 'thumbnail');
		$data['posts'][$i]['featured_image']['medium'] = get_the_post_thumbnail_url($post->ID, 'medium');
        $data['posts'][$i]['featured_image']['large'] = get_the_post_thumbnail_url($post->ID, 'large');
        
        $posttags = get_the_tags($post->ID);
        if ($posttags) {
            $key = 0;
            foreach($posttags as $key => $tag) {
                $data['posts'][$i]['tags'][$key]['id'] = $tag->term_id;
                $data['posts'][$i]['tags'][$key]['name'] = $tag->name;
                $data['posts'][$i]['tags'][$key]['slug'] = $tag->slug;  
            }
        }

        $postcategory = get_the_category($post->ID);
        if ($postcategory) {
            $key = 0;
            foreach($postcategory as $key => $tag) {
                $data['posts'][$i]['categories'][$key]['id'] = $tag->term_id;
                $data['posts'][$i]['categories'][$key]['name'] = $tag->name;
                $data['posts'][$i]['categories'][$key]['slug'] = $tag->slug;  
            }
        }

        // comments
        $data['posts'][$i]['comments']['count'] = get_comments_number($post->ID);

        $i++;
    }
    
	return $data;
}