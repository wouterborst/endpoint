<?php 

function wl_pages() {
	$args = [
		'numberposts' => 10,
		'post_type' => 'page'
    ];
    
	$posts = get_posts($args);
	$data = [];
    
    $i = 0;
	foreach($posts as $post) {
		$data[$i]['id'] = $post->ID;
        $data[$i]['title'] = $post->post_title;
		$data[$i]['slug'] = $post->post_name;
		$data[$i]['featured_image']['thumbnail'] = get_the_post_thumbnail_url($post->ID, 'thumbnail');
		$data[$i]['featured_image']['medium'] = get_the_post_thumbnail_url($post->ID, 'medium');
        $data[$i]['featured_image']['large'] = get_the_post_thumbnail_url($post->ID, 'large');
        
        $posttags = get_the_tags($post->ID);
        if ($posttags) {
            $key = 0;
            foreach($posttags as $key => $tag) {
                $data[$i]['tags'][$key]['id'] = $tag->term_id;
                $data[$i]['tags'][$key]['name'] = $tag->name;
                $data[$i]['tags'][$key]['slug'] = $tag->slug;  
            }
        }

        $postcategory = get_the_category($post->ID);
        if ($postcategory) {
            $key = 0;
            foreach($postcategory as $key => $tag) {
                $data[$i]['categories'][$key]['id'] = $tag->term_id;
                $data[$i]['categories'][$key]['name'] = $tag->name;
                $data[$i]['categories'][$key]['slug'] = $tag->slug;  
            }
        }

		$i++;
	}
	return $data;
}

function wl_page( $slug ) {
	$args = [
		'name' => $slug['slug'],
		'post_type' => 'page'
	];
    $post = get_posts($args);
    $id = $post[0]->ID;

	$data['id'] = $post[0]->ID;
    $data['title'] = $post[0]->post_title;
    $data['slug'] = $post[0]->post_name;
    $data['date_modified'] = get_the_modified_date( 'd-m-Y', $id );
    $data['featured_image'] = show_featured_image($id);

    $dataBlocks = parse_blocks($post[0]->post_content);
    $newBlocks = [];

    if ($dataBlocks) {
            
        $key = 0;
        foreach($dataBlocks as $item) {
            if ($item['blockName'] !== null) {
                array_push($newBlocks, $item);
            }
        }
    }
    $data['blocks'] = $newBlocks;

    // Get Yoast data
    $post_id = $post[0]->ID;
    $data['seo'] = get_yoast($post_id);

	return $data;
}