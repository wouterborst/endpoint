## Endpoint API

The Endpoint API, is a Custom WordPress API, that delivers Post, Categories

### Todo
- **DONE** Need Yoast Meta data
- **DONE** Want to retrieve the_content() different more as a Block style for the front end
- **DONE** Want to use pages, tags data aswell
- Maybe sepparate Comments data from post
- Support Yoast Seo data for premium and normal plugin
- Show links slug
- Add ebook slug / landing page

### Release notes
#### V1.5.1
- fix paged params
  
#### V1.5
- Add post_count to categories 

### Release notes
#### V1.4.2
- Fix for categories links
- fix for category sri lanka

#### V1.4.1
- Add suport for paramters for **posts**

#### V1.4
- Add paramaters for **Categories** and **Tags** post per page and pages etc

#### V1.3
- Add date_modified to single posts and pages endpoint
- Bugfix for comments. Shows only the comments from post ID
- Fix Blocks, remove blocks where the name is null

### Release notes
#### V1.2.2
- Fix SEO Data

#### V1.2.1
- Wordpress blocks for all single entry's
- Create functions for **Tags**, **Categories**, **Comments** and **Media** that makes it less code in the Endpoint functions
- Only show the_content when it is single, remove from all loops with more post the content section
- Add numberofpost in all endpoints

#### V1.2.1
- Wordpress blocks feature in posts

#### V1.2
- Add YOAST SEO fields to Single post entry
- Separate Post, Pages, Categories and Tags in their own files 

#### V1.1
- Create endpoint for tags and single tag bases on slug
- Edit the link section in single categories

### v1
- Endpoint for post and single post based on slug
- Categories and single categories asswell